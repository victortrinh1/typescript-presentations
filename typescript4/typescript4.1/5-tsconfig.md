# PART 5: tsconfig.json changes

## 1 - paths can be used without baseUrl

```json
{
  "compilerOptions": {
    "paths": {
      "foo": ["../foo"]
    }
  }
}
```

This would previously give the error:

```diff
- Option 'paths" cannot be used without specifying '--baseUrl' option
```

> This would often cause poor paths to be used by auto-imports.

## 2 - checkJs implies allowJs

```json
{
  "compilerOptions": {
    "checkJs": true,
    "allowJs": false
  }
}
```

This would give a compiler error before.
Solution was to imply allowJs when checkJs is true.

## 3 - React 17 JSX factories

Supports React 17's upcoming `jsx` and `jsxs` factory functions:

* react-jsx
* react-jsxdev

Example for production
```json
// ./src/tsconfig.json
{
    "compilerOptions": {
        "module": "esnext",
        //...
        "jsx": "react-jsx"
    },
    "include": ["./**/*"]
}
```

Example for development
```json
// ./src/tsconfig.dev.json
{
    // We can extend
    "extends": "./tsconfig.json",
    "compilerOptions": {
        // overwrite
        "jsx": "react-jsxdev"
    }
}
```