/**
 * Resolve's parameters are no longer optional in Promises
 */

const doSomethingAsync = async(data: () => void) => {
    return 123;
}

// Error
const promise = new Promise(resolve => {
    doSomethingAsync(() => {
        resolve(); // Requires a parameter
    })
})

// Pass the correct argument
const correctArgument = new Promise<number>(resolve => {
    doSomethingAsync(() => {
        resolve(2); // Requires a parameter
    })
})

// If you really doesn't need a return statement
const noReturnStatement = new Promise<void>(resolve => {
    doSomethingAsync(() => {
        resolve(); // Requires a parameter
    })
})