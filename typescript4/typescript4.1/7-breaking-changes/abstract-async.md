# Abstract members can't be marked async

The fix is to remove the async keyword for abstract types
