# --declaration and --outfile requires a package name root

If you add the --declaration and --outfile flag in `tsconfig.json`, you may get the error:

```diff
- The `bundledPackageName` option must be provided when using outfile and node module resolution with declaration emit.
```