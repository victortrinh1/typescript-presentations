/**
 * any/unkown are propagated in Falsy Positions
 */

let foo: unknown;
let somethingElse: { someProp: string } = {someProp: "someProp"};

// Before 4.1, x would automatically be typed of the right hand side.
// Now it will take the left hand side (any or unknown).
let x = foo && somethingElse;

// To fix this issue
let x2 = !!foo && somethingElse;