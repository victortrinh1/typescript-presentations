/** 
 * PART 3: Recursive conditional types
 * 
 * Allows you to have Conditional Types that reference themselves.
 */

 type BuildTuple<Current extends [...T[]], T, Count extends number> = 
    Current["length"] extends Count 
        ? Current 
        : BuildTuple<[T, ...Current], T, Count>;

type Tuple<T, Count extends number> = BuildTuple<[], T, Count>;

// same as
// type StringQuintuple = [string, string, string, string, string];
type StringQuintuple = Tuple<string, 5>;

/**
 * BE CAREFUL: This should be used responsibly and sparingly
 * 
 * This can increase type-checking time.
 * Can also hit a compile-time error.
 */
