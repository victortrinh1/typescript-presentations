/** 
 * PART 1: Template Literal Types
 * 
 * Lets you build string literal types
 */

 /************************************ 
 * Simple example of what it permits
 * 
 * same as 
 * type Greeting = "hello world";
 *************************************/
 type World = "world";
 type Greeting = `hello ${World}`; 

 /************************************ 
 * Cute example
 * 
 * same as 
 * type CatFood = "one fish" | "two first" | "red fish" | "blue fish"; 
 *************************************/
type Color = "red" | "blue";
type Quantity = "one" | "two";

type CatFood = `${Quantity | Color} fish`;

 /************************************ 
 * Better example
 * 
 * // same as
 * // type Alignment = "top-left" | "top-center" | "top-right" | "middle-left" | "middle-center" | "middle-right" | "bottom-left" | "bottom-center" | "bottom-right"
 *************************************/
type VerticalAlignment = "top" | "middle" | "bottom";
type HorizontalAlignment = "left" | "center" | "right";
type Alignment = `${VerticalAlignment}-${HorizontalAlignment}`;

function setAlignment(alignment: Alignment) {
    // ....
}
