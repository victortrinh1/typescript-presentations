/** 
 * PART 2: Key Remapping in Mapped Types
 * 
 * Allows you to re-map keys in mapped types with a new `as` clause
 */

 /************************************ 
 * Simple example of what it lets us do
 * 
 * same as 
 * type CatGetter {
 *  getName: string;
 *  getAge: number;
 * }
 *************************************/
 type Getters<T> = {
     [K in keyof T as `get${Capitalize<string & K>}`]: T[K];
 }

 interface Cat {
     name: string;
     age: number;
     kind: "cat";
 }

 type CatGetter = Getters<Cat>;

 /************************************ 
 * Better example
 * 
 * same as
 * type EventType = {
 *  onClick: (event: Event) => void;
 *  onDoubleclick: (event: Event) => void;
 *  onMousedown: (event: Event) => void;
 *  onMouseup: (event: Event) => void;
 * }
 *************************************/
type EventNames = "click" | "doubleclick" | "mousedown" | "mouseup";
type EventType = {
    [K in EventNames as `on${Capitalize<EventNames>}`]: (event: Event) => void; 
}

// NOTE, this uses new type aliases `Uppercase`, `Lowercase`, `Capitalize`, `Uncapitalize`

 /************************************ 
 * Another example
 * 
 * Similar to Omit
 * 
 * same as
 * type Cat = {
 *  name: string;
 *  age: number;
 * }
 *************************************/
type RemoveKindField<T> = {
    [K in keyof T as Exclude<K, "kind">]: T[K]
}

type BetterCat = RemoveKindField<Cat>;