/** 
 * PART 4: --noUncheckedIndexedAccess flag
 * 
 * Adding the `noUncheckedIndexedAccess` on top of the `strictNullChecks` in tsconfig.json
 * catches more possibly undefined objects making it more safe proof.
 */

 interface Dog {
     id: number;
     name: string;
 }

 const dogs : Dog[] = [{id: 1, name: "TunaCat"}];

// These are errors if the flag is at true
/*****************
 * Example 2
 *****************/
 console.log(dogs[0].name);

/*****************
 * Example 2
 *****************/
const [firstDog] = dogs;
console.log(firstDog.name);


