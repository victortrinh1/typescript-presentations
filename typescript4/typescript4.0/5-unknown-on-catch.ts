/**
 * PART 5: Unknown on catch Clause bindings
 * 
 * We can add type 'unknown' to catch clause variable.
 * Forces the developer to use an if check or cast the variable.
 */

 // Before catch clause variables were always typed as "any"
 try {
     //
 } catch (x) {
     // x has type 'any' - have fun!
     // Not safe, we can do whatever we want! 
    console.log(x.message);
    console.log(x.toUpperCase());
    x++;
    x.yadda.yadda.yadda();
 }

 // We can now specify the type as "unknown" instead
 try {
    //
} catch (x: unknown) {
    //  These are now errors!
    //    console.log(x.message);
    //    console.log(x.toUpperCase());
    //    x++;
    //    x.yadda.yadda.yadda();

    // This works
    // Narrowing down 'x' down to the type 'string'
    if(typeof x === "string") {
        console.log(x.toUpperCase());
    }

    // This works
    // Narrowing down 'x' down to the type 'string'
    if(x instanceof Error) {
        x.message;
    }
}

