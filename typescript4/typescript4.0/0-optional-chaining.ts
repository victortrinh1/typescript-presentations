/** 
 * PART 0: Typescript 3.7 + typescript 4.0
 * 
 * You can convert to optional chaining with the Visual Code editor directly by selecting the chain 
 * > Right click "Convert to optional chain expression".
 */

const obj: any = "test";
const test = obj.a && obj.a.b && obj.a.b.c && obj.a.b.c.d();
const test2 = obj.a?.b?.c?.d();