/** 
 * PART 1: Comparing return type of the old concat vs new concat
 * 
 * You can hover on oldArray and newArray to see the typings.
 * Notice the changes when you change the types of the tuples also.
 */

type Arr = readonly any[];

// Tuples
const tuple1: [number, number] = [1,2];
const tuple2: [number, number] = [3,4];

// Old concat
function oldConcat<T, U>(array1: T[], array2: U[]): Array<T | U> {
    return [...array1, ...array2];
}

// New concat
function concat<T extends Arr, U extends Arr>(array1: T, array2: U): [...T, ...U] {
    return [...array1, ...array2];
}

const oldArray = oldConcat(tuple1, tuple2);
const newArray = concat(tuple1, tuple2);
