/**
 * PART 2: Provide labels to tuple types - Purely for documentation and tooling - To know what each element means.
 * 
 * Hover on the typing to see what each element means.
 */

type LabeledTypedTuple = [start: number, end: number];

const labeledTypedTuple: LabeledTypedTuple = [1, 2];