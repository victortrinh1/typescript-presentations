/**
 * PART 3: Class property inference from constructors
 * 
 * IMPORTANT: Requires noImplicitAny
 */

class Person {
    // Previously: implicit any!
    // Now: inferred to 'string'!
    fullName;

    constructor(firstName: string, lastName: string) {
        this.fullName = `${firstName} ${lastName}`;
    }
}