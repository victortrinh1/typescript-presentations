/**
 * PART 4: Short-Circuiting Assignment Operators
 * 
 * New assignments operators: &&=, ||= and ??=
 * 
 * Great for substituting code like:
 * 
 * a = a && b;
 * a = a || b;
 * a = a ?? b;
 */

 let a: string;

 // a = a || "hello";
 a ||= "hello";

 console.log("Exemple 1 : " + a);

 // a = a && "world";
 // Expects "world" because javascript is promising you that it will verify that both sides
 // of the expression are true. In this case, "world" is the last evaluated thing.
 a &&= "world";

 console.log("Exemple 2 : " + a);

// a = a ?? foo;
 a ??= "foo";

console.log("Exemple 3 : " + a);




 